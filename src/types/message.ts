import { generateId } from '../helpers/process.helper';

export class Message {
  constructor(
    public readonly header: string, 
    public readonly body: string = '',
    public readonly id: string = generateId(),
    public readonly message_id: string | null = null,
    public readonly chat_id: string | null = null
  ) {}
}