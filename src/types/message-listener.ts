import { Context } from "telegraf";

export abstract class MessageListener {
  public abstract execute(ctx: Context): void;
  public abstract check(ctx: Context): boolean;

  public doExecute(ctx: Context) {
    if (this.check(ctx)) {
      this.execute(ctx);
      return true;
    }
    return false;
  }
}

