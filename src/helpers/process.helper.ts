import { v4 as uuid } from 'uuid';

export const generateId: () => string = uuid;

export const countAppearances = (text: string | null, lookFor: string) => {
  if (!text) {
    return 0;
  }
  const reg: RegExp = new RegExp(lookFor, 'gim');
  const matched: RegExpMatchArray | null = text.match(reg);
  if (!matched) {
    return 0;
  }

  return matched.length;
}

export const calcResult = (headerAppearances: number, bodyAppearances: number) => headerAppearances * 6 + bodyAppearances;

export const deleteExtraSpacing = (text: string): string => {
  let str: string = '';
  let prevWereSpace: boolean = false;

  for (const char of text) {
    if (!prevWereSpace || !/\s/.test(char)) {
      str += char;
    }

    if (/\s/.test(char) && char !== '\n') {
      prevWereSpace = true;
    }
    else {
      prevWereSpace = false;
    }
  }
  return str;
}