import dotenv from 'dotenv';
dotenv.config();

export const BOT_TOKEN: string = process.env.BOT_TOKEN!;
export const URL: string | undefined = process.env.URL;
export const PORT: string = process.env.PORT || '5000';
export const MODE: mode = process.env.NODE_ENV as mode;

type mode = 'production' | 'development' | 'test';