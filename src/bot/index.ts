import Telegraf, { Context } from "telegraf";
import { BOT_TOKEN, URL, PORT, MODE } from '../config/config';
import { MessageListener } from '../types/message-listener';
import { UpdateType } from "telegraf/typings/telegram-types";

class Bot {
	private bot: Telegraf<Context>;
	private messageListeners: MessageListener[] = [];

	constructor(token: string) {
		this.bot = new Telegraf(token);
		this.bot.on('message', (ctx: Context) => {
			this.messageListeners.some((lister: MessageListener) => lister.doExecute(ctx))
		});

		this.launchBot();
	}

	on(action: UpdateType, cb: MessageListener) {
		this.bot.on(action, (ctx: Context) => cb.execute(ctx));
		return this;
	}

	onMessage(cb: MessageListener) {
		this.messageListeners.push(cb);
		return this;
	}

	launchBot(): void {
		if (MODE === 'development') {
			this.bot.launch();
		}
		if (MODE === 'production') {
			this.bot.telegram.setWebhook(`${URL}/bot${BOT_TOKEN}`);
			this.bot.startWebhook(`/bot${BOT_TOKEN}`, null, PORT as any);
		}
	}
}

export const bot = new Bot(BOT_TOKEN);