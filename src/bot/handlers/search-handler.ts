import { Context } from "telegraf";
import { MessageListener } from "../../types/message-listener";
import { Message } from "../../types/message";
import { messageService } from "../../services/messages-service";
import { countAppearances, calcResult } from "../../helpers/process.helper";
import { BOT_RESPONSES } from "../response-constants";

export class SearchHandler extends MessageListener {
    public async execute(ctx: Context): Promise<void> {
        if (!ctx.message?.text) {
            ctx.reply(BOT_RESPONSES.NO_TEXT);
            return;
        };

        const messages: Message[] = await messageService.getAll();
        const lookFor: string = ctx.message.text;

        let bestMatch: Message = null!;
        let bestScore = 0;
        for (const message of messages) {
            const headerAppearances: number = countAppearances(message.header, lookFor);
            const bodyAppearances: number = countAppearances(message.body, lookFor);

            const score: number = calcResult(headerAppearances, bodyAppearances);

            if (score > bestScore) {
                bestScore = score;
                bestMatch = message;
            }
        }

        if (!bestMatch || !bestScore) {
            ctx.reply(BOT_RESPONSES.NO_RESULT);
            return;
        }

        if (bestMatch.body) {
            this.replyWithText(ctx, bestMatch, lookFor);
        }
        else if (bestMatch.chat_id && bestMatch.message_id) {
            this.replyWithMedia(ctx, bestMatch);
        }
        else {
            ctx.reply(BOT_RESPONSES.NO_RESULT);
        }
    }

    // it's violating single responsibility principle, but don't care
    public formatMessage(text: string, lookFor: string): string {
        const regexp: RegExp = new RegExp(lookFor + '?', 'gim');
        const boldText = text.replace(regexp, (match: string) => '*' + match + '*');
        return boldText;
    }

    public check(ctx: Context) {
        return true;
    }

    public replyWithText(ctx: Context, message: Message, lookFor: string): void {
        const formatted: string = this.formatMessage(message.body.trim(), lookFor);
        ctx.replyWithMarkdown(formatted);
    }

    public replyWithMedia(ctx: Context, message: Message): void {
        ctx.telegram.forwardMessage(ctx.chat!.id!, message.chat_id!, message.message_id!);
    }
}