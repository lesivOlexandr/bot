import { Context } from "telegraf";
import { MessageListener } from "../../types/message-listener";
import { BOT_RESPONSES } from "../response-constants";
import { messageService } from "../../services/messages-service";
import { Message } from "../../types/message";

let id = null;

export class AnswerMessageHandler extends MessageListener {
    public execute(ctx: Context): void {
        if (ctx.message?.text) {
            this.handleTextAdd(ctx);
        }

        if (ctx.message?.caption) {
            this.handleMediaAdd(ctx);
        }
    }

    public check(ctx: Context): boolean {
        return (
            ctx.message?.text?.toLowerCase().startsWith('добавить ответ:') ||
            ctx.message?.caption && true ||
            false
        );
    }

    private async handleTextAdd(ctx: Context): Promise<void> {
        const messageText: string = ctx.message!.text!;
        const wholeMessage: string = messageText.trim();
        
        if (!wholeMessage) {
            ctx.reply(BOT_RESPONSES.USER_MESSAGE_EMPTY);
            return;
        } 

        let messageHeader: string = '';
        for (const char of wholeMessage) {
            if (char !== '\n') {
                messageHeader += char;
            } else {
                break;
            }
        }

        const messageBody: string = wholeMessage.substring(messageHeader.length);

        await messageService.addMessage(new Message(messageHeader, messageBody));
        ctx.reply(BOT_RESPONSES.ANSWER_ADDED);
    }

    private async handleMediaAdd(ctx: Context): Promise<void> {
        const caption: string = ctx.message!.caption!;
        const actualCaption: string = caption.trim();

        if (!caption.trim()) {
            ctx.reply(BOT_RESPONSES.NO_MEDIA_CAPTION);
            return;
        }

        const message_id: string = String(ctx.message!.message_id!);
        const chat_id: string = String(ctx.chat!.id!);
        const message: Message = new Message(actualCaption, undefined, undefined, message_id, chat_id);
        
        await messageService.addMessage(message);
        ctx.reply(BOT_RESPONSES.ANSWER_ADDED);
    }
}