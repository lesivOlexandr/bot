const responses = {
  NO_TEXT: 'You should provide a text for search query',
  NO_MEDIA_CAPTION: 'You should provide a header for media content',
  NO_RESULT: 'Sorry, nothing is there',
  USER_MESSAGE_EMPTY: 'Please, input message',
  ANSWER_ADDED: 'Your answer successfully added',
};

export const BOT_RESPONSES = Object.freeze(responses);
