import { bot } from './bot';
import { AnswerMessageHandler } from './bot/handlers/answer-handler';
import { SearchHandler } from './bot/handlers/search-handler';

bot.onMessage(new AnswerMessageHandler)
bot.onMessage(new SearchHandler);
