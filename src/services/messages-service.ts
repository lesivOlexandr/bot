import fs from 'fs';
import path from 'path';
import { Message } from "../types/message";
import { MODE } from "../config/config";

export class MessageService {
    private messages: Message[] = [];

    constructor(){ this.init() } 

    public async getAll(): Promise<Message[]> {
        return [...this.messages];
    }

    public async addMessage(message: Message): Promise<Message> {
        this.messages.push(message);
        const fileName: string = (MODE === 'test' ? 'messages.test.json': 'messages.json');
        const filePath: string = path.resolve(__dirname, 'storage', fileName);

        fs.writeFileSync(filePath, JSON.stringify(this.messages));
        return {...message};
    }

    public async deleteMessage(message: Message): Promise<Message> {
        this.messages = this.messages.filter(oneMessage => oneMessage.id !== message.id);
        const fileName: string = (MODE === 'test' ? 'messages.test.json': 'messages.json');
        const filePath: string = path.resolve(__dirname, 'storage', fileName);

        fs.writeFileSync(filePath, JSON.stringify(this.messages));
        return {...message};
    }

    public async getOne(id: string): Promise<Message | void> {
        return this.messages.find(message => message.id === id);
    }

    private init(): MessageService {
        if (MODE === 'test') {
            this.messages = require('./storage/messages.test.json')
        }
        if (MODE !== 'test') {
            this.messages = require('./storage/messages.json');
        }
        return this;
    } 
}

export const messageService = new MessageService; 