import { AnswerMessageHandler } from "../src/bot/handlers/answer-handler"
import { deleteExtraSpacing } from "../src/helpers/process.helper";

test('add message to be triggered', () => {
  const answerHandler: AnswerMessageHandler = new AnswerMessageHandler;
})

test('Function correctly removes extra spacing', () => {
  const result: string = deleteExtraSpacing('  ');
  expect(result).toBe(' ');

  const result2: string = deleteExtraSpacing('   ');
  expect(result2).toBe(' ');

  const result3: string = deleteExtraSpacing('text');
  expect(result3).toBe('text');

  const result4: string = deleteExtraSpacing('text  text');
  expect(result4).toBe('text text');

  const result5: string = deleteExtraSpacing('text\n\n\ntext');
  expect(result5).toBe('text\n\n\ntext');
})