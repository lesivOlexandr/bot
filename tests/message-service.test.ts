import { messageService } from '../src/services/messages-service';
import { Message } from '../src/types/message';

test('Message service returns messages', async () => {
    const messages: Message[] = await messageService.getAll();
    const expectedMessage: Message = new Message('some test header', 'some text body', 'testId');
    expect(messages).toEqual([expectedMessage]);
});

test('Message service gets a message by it\'s id', async () => {
    const message: Message | void = await messageService.getOne('testId');
    expect(message).toBeTruthy();
    expect(message).toEqual(new Message('some test header', 'some text body', 'testId'));
})

test('Message service adds messages', async () => {
    const messagesBefore: Message[] = await messageService.getAll();
    const toAdd: Message = new Message('test', 'example', 'test');
    await messageService.addMessage(toAdd);

    const messagesAfter: Message[] = await messageService.getAll();
    expect([...messagesBefore, toAdd]).toEqual(messagesAfter);
});

// should be ran after test of adding messages
test('Message service removes messages', async () => {
    const messagesBefore: Message[] = await messageService.getAll();
    const toDelete: Message = new Message('test', 'example', 'test');
    await messageService.deleteMessage(toDelete);

    const messagesAfter: Message[] = await messageService.getAll();
    expect(messagesBefore.filter(m => m.id !== toDelete.id)).toEqual(messagesAfter);
});
