import { countAppearances } from '../src/helpers/process.helper';
import { SearchHandler } from '../src/bot/handlers/search-handler';
import { BOT_RESPONSES } from '../src/bot/response-constants';

test('function counts appearances of substring correctly', () => {
    expect(countAppearances('test', 'test')).toBe(1);
    expect(countAppearances('test test', 'test')).toBe(2);
    expect(countAppearances('test', 'no match')).toBe(0);

    expect(countAppearances('TESt', 'TesT')).toBe(1);
});

test('search handler returns error message, if no match text provided', () => {
    const searchHandler: SearchHandler = new SearchHandler;
    searchHandler.execute({
        message: { text: '' },
        reply: function(response: string | void) { 
            expect(response).toBeTruthy();
            expect(response).toBe(BOT_RESPONSES.NO_TEXT) }
        } as any);
});

test('search handler returns a message with results if it found', () => {
    const searchHandler = new SearchHandler;
    searchHandler.execute({
        message: { text: 'text' },
        reply: function(response: string | void) {
            expect(response).toBeTruthy();
        }
    } as any)
})

test('search handler returns a message, if no result found', () => {
    const searchHandler = new SearchHandler;
    searchHandler.execute({
        message: { text:'no matches' },
        reply: function(response: string | void) {
            expect(response).toBeTruthy();
            expect(response).toBe(BOT_RESPONSES.NO_RESULT)
        }
    } as any)
})

test('function formats text correctly', () => {
    const searchHandler: SearchHandler = new SearchHandler;
    const formatted1: string = searchHandler.formatMessage('text', 'tex');
    const formatted2: string = searchHandler.formatMessage('text', 'non-match');

    expect(formatted1).toBe('*tex*t');
    expect(formatted2).toBe('text');
});
